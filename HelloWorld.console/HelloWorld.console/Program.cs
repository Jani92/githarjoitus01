﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld.console
{
    class Program
    {
        static void Main(string[] args)
        { 
            // Tuulostaa tekstin Hello Git
            Console.WriteLine("Hello Git!");
            Console.WriteLine("Yo");

            Laskin laskin1 = new Laskin();
            int summa = laskin1.Summa(5, 6);
            Console.WriteLine("Summa: " + summa );

            int summa2 = laskin1.Summa(3, 12);
            Console.WriteLine("Summa: " + summa2);

            int summa3 = laskin1.Summa(4, 7);
            Console.WriteLine("Summa: " + summa3);

        }
    }
}
